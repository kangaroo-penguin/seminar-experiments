# This module follows https://www.tensorflow.org/programmers_guide/reading_data#reading_from_files
# as well as https://github.com/tensorflow/models/blob/master/tutorials/image/cifar10/cifar10_input.py

import os
import tensorflow as tf
from global_vars import *
from config import *

class CIFAR10Record():
    """Represents a single CIFAR10 image.
    """
    def __init__(self, label, image, data_format):

        """The label of this record represented as tf.int32 in the range 0 to 9.
        """
        self.label = label

        """The image of this record represented as tensor of dtype FLOAT and
        shape [self.heigth, self.width, self.channels] or
        [self.channels, self.heigth, self.width], depending on the data format
        specified in self.data_format.
        """
        self.image = image

        """The width of the image wrapped by this record.
        """
        self.width = IMAGE_SIZE

        """The height of the image wrapped by this record.
        """
        self.heigth = IMAGE_SIZE

        """The number of channels of the image wrapped by this record.
        """
        self.channels = IMAGE_CHANNELS

        """The format of the image wrapped by this record.
        See the note on CHW and HWC for details.
        """
        if data_format != "CHW" and data_format != "HWC":
            raise ValueError("CIFAR10Record(): Invalid data format!")
        self.data_format = data_format

def readRecord(queue, data_format = "HWC"):
    """Reads a single CIFAR10 record from the specified queue.

    Args:
        queue: The queue to read from.
        data_format: The data format of the returned CIFAR10 record; String
                     from "CHW" and "HWC" (default).

    Returns:
        A single CIFAR10 record.
    """
    # We perform the following steps:
    # * Read a single record from the specified queue
    # * Extract the label and image data while casting to appropriate datatypes
    # * Reshape the image from CHW to HWC representation if needed
    reader = tf.FixedLengthRecordReader(RECORD_BYTES)
    _, raw_record = reader.read(queue)
    int_record = tf.decode_raw(raw_record, tf.uint8)
    label = tf.cast(tf.strided_slice(int_record, [0], [LABEL_BYTES]),
                    tf.int32)
    image_CHW = tf.reshape(tf.strided_slice(int_record, [LABEL_BYTES],
                                            [RECORD_BYTES]),
                           [IMAGE_CHANNELS, IMAGE_SIZE, IMAGE_SIZE])
    image_CHW = tf.cast(image_CHW, FLOAT)

    if data_format == "CHW":
        return CIFAR10Record(label, image_CHW, data_format)
    elif data_format == "HWC":
        image_HWC = tf.transpose(image_CHW, perm = [1, 2, 0])
        return CIFAR10Record(label, image_HWC, data_format)
    else:
        raise ValueError("readRecord(): Invalid data format")

def getFilenameQueue(input_dir, test_data = False, num_epochs = None):
    """Returns a filename queue holding the training/test data filenames.

    Args:
        input_dir: The directory containing the binary data.
        test_data: Boolen flag indicating whether to fetch training or test
                   data. True corresponds to test data.
        num_epochs: Int indicating how often every record should be produced
                    before raising an 'OutOfRangeError'. 'None' disables this
                    upper bound.

    Returns:
        A filename queue holding the data filenames.
    """
    if test_data:
        filenames = [os.path.join(input_dir, "test_batch.bin")]
    else:
        filenames = [os.path.join(input_dir, "data_batch_%d.bin" % i)
                     for i in range(1, NUM_FILES + 1)]

    for f in filenames:
        if not tf.gfile.Exists(f):
            raise ValueError("Unable to locate " + f)

    queue = tf.train.string_input_producer(filenames, num_epochs = num_epochs)
    return queue

def input(input_dir, batch_size, data_format = "NHWC", shuffle = False,
          test_data = False, num_epochs = None):
    """Construct CIFAR10 input using readers.

    Args:
        input_dir: The directory containing the binary data.
        batch_size: Int indicating the number of records per batch.
        data_format: The data format of the returned records; String from "NCHW",
                     "NHWC" (default).
        shuffle: Boolean indicating whether to shuffle the records read from the
                 binary files or not.
        test_data: Boolean indicating whether to read training data or test
                   data.
        num_epochs: An optional integer indicating the maximum number of epochs,
                    if specified.

    Returns:
        images: 4D tensor of shape
                [batch_size, IMAGE_SIZE, IMAGE_SIZE, IMAGE_CHANNELS]
                representing the images.
        labels: 1D tensor of shape [batch_size] representing the corresponding
                ground truth.
    """
    queue = getFilenameQueue(input_dir, test_data, num_epochs)
    record = readRecord(queue, data_format[1:])

    # Set shapes.
    record.label.set_shape([1])

    if test_data:
        num_records = NUM_TEST_RECORDS
    else:
        num_records = NUM_TRAIN_RECORDS

    min_after_dequeue = int(MIN_PREFETCHED_FRACTION * num_records)
    capacity = min_after_dequeue + MAX_PREFETCHED_BATCHES * batch_size

    if shuffle:
        images, labels = tf.train.shuffle_batch(
                [record.image, record.label],
                batch_size = batch_size,
                num_threads = NUM_PREPROCESS_THREADS,
                min_after_dequeue = min_after_dequeue,
                capacity = capacity)
    else:
        images, labels = tf.train.batch(
                [record.image, record.label],
                batch_size = batch_size,
                num_threads = NUM_PREPROCESS_THREADS,
                capacity = capacity)

    return images, tf.reshape(labels, [batch_size])
