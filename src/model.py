import tensorflow as tf
import cifar10_input as cifar10
from global_vars import *
from config import *
from math import sqrt

def _instanciate_var(name, shape, initializer, decay = None, trainable = True):
    """Helper to create a Variable.
    Args:
        name: Name of the Variable to instanciate.
        shape: The shape of the Variable represented as list of integers.
        initializer: The initializer of the Variable to instanciate.
        decay: The strength of L2Loss. If 'None', no L2Loss is added.
        trainable: Boolean indicating whether the returned variable is
                   trainable; Defaults to 'True'.

    Returns:
        The variable tensor.
    """
    var = tf.get_variable(name = name, shape = shape,
                          initializer = initializer,
                          trainable = trainable,
                          dtype = FLOAT)
    if decay is not None:
        wd = tf.multiply(tf.nn.l2_loss(var), decay, name = name + "-L2_loss")
        tf.add_to_collection("losses", wd)
    return var

def _conv2d(input_tensor, shape, strides = [1, 1, 1, 1]):
    """Set up the convolutional filtering part of a convolutional layer.
    Note that this method does not perform variable scope handling!

    Args:
        input_tensor: Tensor representing the input of this convolutional layer.
                      Must be of shape [batch_size, height, width, in_channels]
        shape: List of ints indicating the number and size of filters, i.e.
               [filter_height, filter_width, in_channels, out_channels].
        strides: List of ints indicating the filter strides, i.e.
                 [1, horizontal_stride, vertical_stride, 1]; Defaults to
                 [1, 1, 1, 1].

    Returns:
        Convolutionally filtered input tensor.
    """
    stddev = sqrt(2.0 / (shape[0] * shape[1] * shape[-1]))
    weight_init = tf.truncated_normal_initializer(stddev = stddev,
                                                  dtype = FLOAT)
    weights = _instanciate_var("weights",
                               shape,
                               weight_init,
                               WEIGHT_DECAY)
    return tf.nn.conv2d(input_tensor, weights, strides, "SAME")

def _batch_norm_per_channel_relu(input_tensor, channels, mode_eval):
    """Performs per channel batch normalization of the specified input
    followed by an application of the ReLU activation function.
    Note that this method does not perform variable scope handling!

    Args:
        input_tensor: Tensor representing the values to normalize.
                      Must be of shape
                      [batch_size, height, width, channels].
        channels: Int indicating the number of channels in input_tensor.
        mode_eval: Boolean indicating whether the returned layer is used as
                   part of a training model ('False') or evaluation model
                   ('True').

    Returns:
        Tensor representing the input after performing per channel batch
        normalization as well as applying the ReLU activation function.
    """
    # We apply batch normalization as described in [2].
    # Have a look at the documentations of 'tf.nn.moments()',
    # 'tf.nn.batch_normalization()' and 'tf.nn.fused_batch_norm()'
    scale = _instanciate_var("scale", [channels], SCALE_INIT)
    offset = _instanciate_var("offset", [channels], OFFSET_INIT)

    pop_mean = _instanciate_var("pop_mean", [channels], MEAN_INIT,
                                trainable = False)
    pop_variance = _instanciate_var("pop_variance", [channels],
                                    VARIANCE_INIT,
                                    trainable = False)

    # We have to distinguish training and evaluation here.
    # Note: a use of tf.train.ExponentialMovingAverage is desirable for
    # updating the population mean and variance. I didn't get this to work,
    # therefor the solution presented in [3] is used. For evaluation,
    # variables have to be initialized using a training session checkpoint
    # file!
    if mode_eval:
        bn_conv, _, _ = tf.nn.fused_batch_norm(input_tensor,
                        scale = scale,
                        offset = offset,
                        mean = pop_mean,
                        variance = pop_variance,
                        epsilon = EPSILON,
                        is_training = False)
        activation = tf.nn.relu(bn_conv, name = "activations")
    else:
        bn_conv, batch_mean, batch_var = tf.nn.fused_batch_norm(
                input_tensor, scale = scale,
                offset = offset, epsilon = EPSILON)
        train_mean = tf.assign_sub(pop_mean,
                (1 - MOVING_AVERAGE_DECAY) * (pop_mean - batch_mean))
        train_var = tf.assign_sub(pop_variance,
                (1 - MOVING_AVERAGE_DECAY) * (pop_variance - batch_var))

        with tf.control_dependencies([train_mean, train_var]):
            activation = tf.nn.relu(bn_conv, name = "activations")
    return activation

def _conv_layer(input_tensor, scope, shape, mode_eval,
                strides = [1, 1, 1, 1], response_collection = False):
    """Sets up a batch normalized convolutional layer consisting of a pipe of
    convolution, batch normalization and relu.

    Args:
        input_tensor: Tensor representing the input of this convolutional layer.
                      Must be of shape [batch_size, height, width, in_channels]
        scope: A string used as name of the scope.
        shape: List of ints indicating the number and size of filters, i.e.
               [filter_height, filter_width, in_channels, out_channels].
        mode_eval: Boolean indicating whether the returned layer is used as part
                   of a training model ('False') or evaluation model
                   ('True').
        strides: List of ints indicating the filter strides, i.e.
                 [1, horizontal_stride, vertical_stride, 1]; Defaults to
                 [1, 1, 1, 1].
        response_collection: Boolean indicating whether to add this
                             convolutional layer response to the building block
                             response collection ('True') or not
                             ('False', default).

    Returns:
        Tensor of activations of specified convolutional layer.
    """
    with tf.variable_scope(scope):
        # First of all, we apply the convolution(s).
        conv = _conv2d(input_tensor, shape, strides = [1, 1, 1, 1])

        if response_collection:
            tf.add_to_collection("building_block_responses", tf.reduce_mean(tf.abs(conv)))

        # Now, we apply batch normalization.
        return _batch_norm_per_channel_relu(conv, shape[-1], mode_eval)

def _residual_building_block(input_tensor, scope, channels, mode_eval):
    """Sets up a single residual building block consisting of two subsequent
    convolutional layers leaving the number of channels invariant.
    The application of the second non-linearity is performed after adding
    the identity mapping as shown in Fig. 2 in [4].

    Args:
        input_tensor: Tensor representing the input of this building block.
                      Must be of shape [batch_size, height, width, in_channels]
        scope: A string used as name of the scope.
        channels: Int indicating the number of filters applied at each
                  convolutional layer.
        mode_eval: Boolean indicating whether the returned block is used as part
                   of a training model ('False') or evaluation model
                   ('True').

    Returns:
        Tensor of activations of the last wrapped convolutional layer.
    """
    conv_1 = _conv_layer(input_tensor, scope + "_conv_1",
                         [3, 3, channels, channels],
                         mode_eval = mode_eval)

    with tf.variable_scope(scope + "_conv_2"):
        conv_2 = _conv2d(conv_1, [3, 3, channels, channels])
        tf.add_to_collection("building_block_responses", tf.reduce_mean(tf.abs(conv_2)))
        shortcut = tf.add(conv_2, input_tensor)
        return _batch_norm_per_channel_relu(shortcut, channels, mode_eval)

def _plain_building_block(input_tensor, scope, channels, mode_eval):
    """Sets up a single building block consisting of two subsequent
    convolutional layers leaving the number of channels invariant.

    Args:
        input_tensor: Tensor representing the input of this building block.
                      Must be of shape [batch_size, height, width, in_channels]
        scope: A string used as name of the scope.
        channels: Int indicating the number of filters applied at each
                  convolutional layer.
        mode_eval: Boolean indicating whether the returned block is used as part
                   of a training model ('False') or evaluation model
                   ('True').

    Returns:
        Tensor of activations of the last wrapped convolutional layer.
    """
    conv_1 = _conv_layer(input_tensor, scope + "_conv_1",
                         [3, 3, channels, channels],
                         mode_eval = mode_eval)
    conv_2 = _conv_layer(conv_1, scope + "_conv_2",
                         [3, 3, channels, channels],
                         mode_eval = mode_eval,
                         response_collection = True)
    return conv_2

def _n_residual_building_blocks(input_tensor, num_blocks, scope, channels,
                             mode_eval):
    """Sets up n subsequent residual building blocks.

    Args:
        input_tensor: Tensor representing the input of this building block
                      level.
                      Must be of shape [batch_size, height, width, in_channels]
        scope: A string used as name of the scope.
        channels: Int indicating the number of filters applied at each wrapped
                  convolutional layer.
        mode_eval: Boolean indicating whether the returned blocks are used as
                   part of a training model ('False') or evaluation model
                   ('True').

    Returns:
        Tensor of activations of the last wrapped convolutional layer.
    """
    if num_blocks < 1:
        raise ValueError("_n_residual_building_blocks(): invalid number of"
                         " blocks %d" % num_blocks)
    bb = input_tensor

    for i in range(num_blocks):
        bb = _residual_building_block(bb, scope + "_block_" + str(i), channels,
                                      mode_eval)

    return bb

def _n_plain_building_blocks(input_tensor, num_blocks, scope, channels,
                             mode_eval):
    """Sets up n subsequent plain building blocks.

    Args:
        input_tensor: Tensor representing the input of this building block
                      level.
                      Must be of shape [batch_size, height, width, in_channels]
        scope: A string used as name of the scope.
        channels: Int indicating the number of filters applied at each wrapped
                  convolutional layer.
        mode_eval: Boolean indicating whether the returned blocks are used as
                   part of a training model ('False') or evaluation model
                   ('True').

    Returns:
        Tensor of activations of the last wrapped convolutional layer.
    """
    if num_blocks < 1:
        raise ValueError("_n_residual_building_blocks(): invalid number of"
                         " blocks %d" % num_blocks)
    bb = input_tensor

    for i in range(num_blocks):
        bb = _plain_building_block(bb, scope + "_block_" + str(i), channels,
                                      mode_eval)

    return bb

def _spatial_size_to_filter_num_block(input_tensor, scope, channels, mode_eval):
    """Sets up a block consisting of a 2 by 2 max pooling layer followed by a 1
    by 1 convolutional layer to half spatial size and double the number of
    channels.

    Args:
        input_tensor: Tensor representing the input of this block.
                      Must be of shape [batch_size, height, width, in_channels]
        scope: A string used as name of the scope.
        channels: List of ints indicating the number of channels of input_tensor
                  and the desired number of channels of the returned tensor,
                  i.e. [in_channels, out_channels].
        mode_eval: Boolean indicating whether the returned block is used as part
                   of a training model ('False') or evaluation model
                   ('True').

    Returns:
        Tensor of activation of the wrapped convolutional layer.
    """
    pooling = tf.nn.max_pool(input_tensor, [1, 2, 2, 1], [1, 2, 2, 1], "VALID")
    conv = _conv_layer(pooling, scope + "_conv", shape = [1, 1] + channels,
                       mode_eval = mode_eval)
    return conv

def _tail(input_tensor, shape):
    """Sets up the tail, i.e. the topmost layers, of the inference
    model, consisting of a global average pooling layer followed by a 10 way
    fully connected layer. The remaining softmax layer has to be applied by
    hand!

    Args:
        input_tensor: Tensor representing the input of this block.
                      Must be of shape [batch_size, height, width, in_channels]
        shape: List of ints indicating the input feature map size, i.e.
               [height, width].

    Returns:
        Tensor representing the logits of this inference model.
    """
    with tf.variable_scope("tail"):
        # Perform global average pooling.
        avg = tf.nn.avg_pool(input_tensor,
                             [1] + shape + [1],
                             [1, 1, 1, 1], "VALID")
        avg_mat = tf.reshape(avg, [-1, CHANNELS[-1]])

        # Pipe tensor through an 10 way fully connected layer.
        stddev = sqrt(2.0 / CHANNELS[-1])
        weight_init = tf.truncated_normal_initializer(stddev = stddev,
                                                      dtype = FLOAT)

        weights =  _instanciate_var("weights",
                                    [CHANNELS[-1], NUM_CLASSES],
                                    weight_init, decay = WEIGHT_DECAY)
        mm = tf.matmul(avg_mat, weights)

        biases = _instanciate_var("biases", [NUM_CLASSES], OFFSET_INIT)
        logits = tf.nn.bias_add(mm, biases, name = "logits")
    return logits

def _building_block_responses_summary():
    """Introduces summary ops for the per net mean and variance of the L2 norm
    of building block responses.
    """
    per_tensor_mean_abs = tf.get_collection("building_block_responses")
    mean, variance = tf.nn.moments(tf.stack(per_tensor_mean_abs), axes = [0])
    tf.summary.scalar("building_block_responses_mean", mean)
    tf.summary.scalar("building_block_responses_variance", variance)

def inference(images, blocks_per_level, plain = True, data_format = "NHWC",
              mode_eval = False):
    """Build the model up to where it may be used for classification.
    Note that the remaining softmax layer has to be applied by hand!

    Args:
        input_images: Placeholders representing the input images of shape
                      [BATCH_SIZE, IMAGE_SIZE, IMAGE_SIZE, 3] or
                      [BATCH_SIZE, 3, IMAGE_SIZE, IMAGE_SIZE] depending on the
                      data format.
        blocks_per_level: Int representing the number of building blocks per
                          level of building blocks.
        plain: Boolean indicating whether the returned model uses plain or
               residual building blocks; Defaults to plain building blocks
               ('True').
        data_format: The format of the specified images; String from "NCHW",
                     "NHWC" (default).
        mode_eval: Boolean indicating whether the returned model is used as for
                   training ('False', default) or evaluation ('True').

    Returns:
        scores: Output tensor with the scores computed by this model.
    """
    # Initial convolutional layer.
    # filter size: 1 by 1
    # number of out-channels: CHANNELS[0]
    # stride: 1
    initial_conv = _conv_layer(images, "initial_conv",
                               [1, 1, IMAGE_CHANNELS, CHANNELS[0]],
                               mode_eval = mode_eval)

    # First layer of building blocks.
    # filter size: 3 by 3
    # number of channels: CHANNELS[0]
    # stride: 1
    if plain:
        bbs_0 = _n_plain_building_blocks(initial_conv, blocks_per_level,
                                         "layer_0",
                                         CHANNELS[0],
                                         mode_eval = mode_eval)
    else:
        bbs_0 = _n_residual_building_blocks(initial_conv, blocks_per_level,
                                         "layer_0",
                                         CHANNELS[0],
                                         mode_eval = mode_eval)

    # Half spatial size, double number of filters.
    ss2nf_0 = _spatial_size_to_filter_num_block(bbs_0, "down_sample_0",
                                                CHANNELS[0:2],
                                                mode_eval = mode_eval)

    # Second layer of building blocks.
    # filter size: 3 by 3
    # number of channels: CHANNELS[1]
    # stride: 1
    if plain:
        bbs_1 = _n_plain_building_blocks(ss2nf_0, blocks_per_level,
                                         "layer_1",
                                         CHANNELS[1],
                                         mode_eval = mode_eval)
    else:
        bbs_1 = _n_residual_building_blocks(ss2nf_0, blocks_per_level,
                                         "layer_1",
                                         CHANNELS[1],
                                         mode_eval = mode_eval)

    # Half spatial size, double number of filters again.
    ss2nf_1 = _spatial_size_to_filter_num_block(bbs_1, "down_sample_1",
                                                CHANNELS[1:3],
                                                mode_eval = mode_eval)

    # Third layer of building blocks.
    # filter size: 3 by 3
    # number of channels: CHANNELS[2]
    # stride: 1
    if plain:
        bbs_2 = _n_plain_building_blocks(ss2nf_1, blocks_per_level,
                                         "layer_2",
                                         CHANNELS[2],
                                         mode_eval = mode_eval)
    else:
        bbs_2 = _n_residual_building_blocks(ss2nf_1, blocks_per_level,
                                         "layer_2",
                                         CHANNELS[2],
                                         mode_eval = mode_eval)

    # Add summary ops in case we evaluate the model.
    if mode_eval:
        _building_block_responses_summary()

    # Tail of the model.
    return _tail(bbs_2, [8, 8])

def loss(logits, labels):
    """Set up total loss consisting of average cross-entropy data loss and L2
    regularization loss.

    Args:
        logits: Tensor representing the logits infered by the model; Of shape
                [batch_size, NUM_CLASSES].
        labels: Tensor representing the corresponding ground truth labels; Of
                shape [batch_size].

    Returns:
        Tensor representing the sum of cross-entropy and L2 loss.
    """
    # Calculate average cross entropy loss.
    cross_entropy_per_img = tf.nn.sparse_softmax_cross_entropy_with_logits(
            labels=labels,
            logits=logits,
            name = "cross_entropy_img")
    cross_entropy = tf.reduce_mean(cross_entropy_per_img,
                                   name = "cross_entropy")
    tf.summary.scalar("cross entropy loss", cross_entropy)
    tf.add_to_collection("losses", cross_entropy)

    # Calculate total loss.
    total_loss = tf.add_n(tf.get_collection("losses"), name = "total_loss")
    tf.summary.scalar("total loss", total_loss)
    return total_loss

def train(total_loss, global_num_iter):
    """Set up a training step.

    Args:
        total_loss: Tensor representing the total loss.
        global_num_iter: Tensor indicating the number of iterations performed so
                         far.

    Returns:

    """
    learning_rate = tf.train.piecewise_constant(global_num_iter,
                                                LR_DECAY_BOUNDARIES,
                                                LR_VALUES,
                                                name = "learning_rate")
    tf.summary.scalar("learning rate", learning_rate)
    optimizer = tf.train.MomentumOptimizer(learning_rate, MOMENTUM)
    grads_and_vars = optimizer.compute_gradients(total_loss)

    # Add summary for gradient norms.
    for grad, var in grads_and_vars:
        if grad is not None:
            tf.summary.scalar(var.name + "/L2_norm_of_gradient", tf.norm(grad))

    return optimizer.apply_gradients(grads_and_vars, global_step = global_num_iter)

def top_k(logits, labels, k):
    """Determine whether the ground truth is among the top k predictions.

    Args:
        logits: Tensor representing the logits infered by the model; Of shape
                [batch_size, NUM_CLASSES].
        labels: Tensor representing the corresponding ground truth labels; Of
                shape [batch_size].
        k: Int indicating the number of topmost predictions to consider.

    Returns:
        A boolean Tensor of shape [batch_size]. The i-th entry is 'True' iff
        the i-th ground truth label is among the top k predictions of the
        model.
    """
    probabilities = tf.nn.softmax(logits)
    return tf.nn.in_top_k(probabilities, labels, k)

# References
#
# [1] Delving deep into Rectifiers: Surpassing Human-Level Performance on
# ImageNet Classification, He et al.
#
# [2] Batch Normalization: Accelerating Deep Network Training by Reducing
# Internal Covariate Shift, Ioffe and Szegedy
#
# [3] https://r2rt.com/implementing-batch-normalization-in-tensorflow.html
#
# [4] Deep Residual Learning for Image Recognition, He et al.
