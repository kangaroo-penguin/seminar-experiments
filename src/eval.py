import tensorflow as tf
import numpy as np
import preprocessed_input as data
import model as m
from global_vars import *
from config import *
from train import output_dir

import os
import time
from datetime import datetime
from sys import argv

def evaluate(num_blocks, plain, input_dir, checkpoint_dir, batch_size):
    """Evaluate a plain or residual model with specified number of building
    blocks per level.

    Args:
        num_blocks: Int indicating the number of building blocks to use per
                    level of building blocks.
        plain: Boolean indicating whether to use a plain model ('True') or a
               residual model ('False').
        input_dir: Path to input data files.
        checkpoint_dir: Path to checkpoint files written by train.py.
        batch_size: Int indicating the batch size to use during training.
    """
    global_num_iter = tf.contrib.framework.get_or_create_global_step()

    # First of all, we set up the model to evaluate.
    images, labels = data.input(input_dir, batch_size,
                                            shuffle = True,
                                            data_type = "test",
                                            num_epochs = 1)
    inference = m.inference(images, num_blocks, plain = plain, mode_eval = True)
    top_1 = m.top_k(inference, labels, 1)

    # At this point, we're done with the model. We have to initialize the
    # variables and restore values where needed.
    saver = tf.train.Saver()
    global_init_op = tf.global_variables_initializer()
    local_init_op = tf.local_variables_initializer()
    sess = tf.Session()
    sess.run([global_init_op, local_init_op])
    directory = output_dir(checkpoint_dir, num_blocks, plain)
    saver.restore(sess, tf.train.latest_checkpoint(directory))

    # Start input enqueue threads.
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    # Perform evaluation.
    number_of_steps_performed = sess.run(global_num_iter)

    try:
        correct_count = 0
        while not coord.should_stop():
            correct_count += np.sum(sess.run(top_1))

    except tf.errors.OutOfRangeError:
        # m.input() discards the final batch if smaller than BATCH_SIZE.
        num_records = NUM_TEST_RECORDS - NUM_TEST_RECORDS % BATCH_SIZE
        print("--- Evaluation ---")
        print("reached accuracy %f after %d steps"
                % (correct_count / num_records, number_of_steps_performed))

    finally:
        # When done, ask the threads to stop.
        coord.request_stop()

    # Wait for threads to finish.
    coord.join(threads)
    sess.close()

if __name__ == "__main__":
    from sys import argv

    num_blocks = int(argv[1])
    plain = {"plain": True, "res": False}[argv[2]]

    evaluate(num_blocks, plain, PREPROCESSED_DIR, CHECKPOINT_DIR, BATCH_SIZE)
