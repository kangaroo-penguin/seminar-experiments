"""This module defines global variables describing the general layout of data we
are dealing with throughout the experiments.
"""

import tensorflow as tf
from config import *

###############################################################################
# Main properties of the Cifar10 binary data.
###############################################################################

"""Number of classes the images belong to.
"""
NUM_CLASSES = 10

"""Number of images in binary training data.
"""
NUM_TRAIN_RECORDS = 50000

"""Number of images in test data.
"""
NUM_TEST_RECORDS = 10000

"""Height and width of a single image.
"""
IMAGE_SIZE = 32

"""Number of pixels of a single image.
"""
IMAGE_PIXELS = IMAGE_SIZE * IMAGE_SIZE

"""Number of channels of a single image.
"""
IMAGE_CHANNELS = 3

"""Number of entries in a 3D tensor representing a single image.
"""
IMAGE_ENTRIES = IMAGE_PIXELS * IMAGE_CHANNELS

"""Number of binary input files representing the training data only.
"""
NUM_FILES = 5

"""Size of a label in bytes in the binary representation.
"""
LABEL_BYTES = 1

"""Size of a record in bytes in the binary representation.
"""
RECORD_BYTES = LABEL_BYTES + IMAGE_SIZE * IMAGE_SIZE * IMAGE_CHANNELS

###############################################################################
# Main properties of preprocessed data.
###############################################################################

"""Size of validation fold.
"""
NUM_VAL_RECORDS = 5000

"""Size of preprocessed training fold.
"""
NUM_PREP_TRAIN_RECORDS = NUM_TRAIN_RECORDS - NUM_VAL_RECORDS

"""Number of preprocessed records per .tfrecords-file. Must divide
NUM_TRAIN_RECORDS evenly.
"""
RECORDS_PER_TRAIN_FILE = 5000

"""Base name of the training .tfrecords-files.
"""
TRAIN_BASENAME = "train"

"""Base name of the validation .tfrecords-file.
"""
VAL_BASENAME = "val"

"""Base name of the test .tfrecords-file.
"""
TEST_BASENAME = "test"

"""Suffix of the .tfrecords-files.
"""
FILE_SUFFIX = ".tfrecords"

###############################################################################
# Settings concerning the model layout.
###############################################################################

"""Number of channels in each layer of building blocks.
"""
CHANNELS = [8, 16, 32]

###############################################################################
# Settings concerning training.
###############################################################################

"""Batch size used during training.
"""
BATCH_SIZE = 64

"""L2 regularization strength.
"""
WEIGHT_DECAY = 0.0001

"""The number of epochs of training.
"""
NUM_EPOCHS = 10

# Note: tensorflow ships an initializer
# tf.contrib.keras.initializers.he_normal() which is supposed to do
# initialization according to He et al. as discussed in reference [1].
# Unfortunately this initializer keeps raising errors. Therefore, the initializer
# is manually derived where needed.
# WEIGHT_INIT = tf.contrib.keras.initializers.he_normal()
# WEIGHT_INIT = None

"""Initializer for batch normalization scale.
"""
SCALE_INIT = tf.ones_initializer(dtype=FLOAT)

"""Initializer for batch normalization offset.
"""
OFFSET_INIT = tf.zeros_initializer(dtype=FLOAT)

"""Initializer for batch normalization population mean.
"""
MEAN_INIT = tf.zeros_initializer(dtype=FLOAT)

"""Initializer for batch normalization population variance.
"""
VARIANCE_INIT = tf.ones_initializer(dtype=FLOAT)

"""Batch normalization epsilon.
"""
EPSILON = 0.001

"""Index of iterations at which the learing rate gets decayed.
"""
LR_DECAY_BOUNDARIES = [tf.constant(3906, dtype=tf.int64),
                       tf.constant(5859, dtype=tf.int64)]

"""Learning rate used in the intervals defined by above boundaries.
"""
LR_VALUES = [0.01, 0.005, 0.0025]

"""Momentum used during training.
"""
MOMENTUM = 0.9

"""Decay used for moving average calculation.
"""
MOVING_AVERAGE_DECAY = 0.999

###############################################################################
# Settings concerning prefetching..
###############################################################################

""""Minimal fraction of prefetched records to total number of records. Greater
means slower start-up but better shuffling.
"""
MIN_PREFETCHED_FRACTION = 0.4

"""Maximum number of prefetched batches on top of
MIN_PREFETCHED_FRACTION * total number of records.
"""
MAX_PREFETCHED_BATCHES = NUM_PROCESS_THREADS + 1
