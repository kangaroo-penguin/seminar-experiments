import tensorflow as tf
import preprocessed_input as data
import model as m
from global_vars import *
from config import *

import os
import time
from datetime import datetime
from sys import argv

class _RuntimeLoggerHook(tf.train.SessionRunHook):
  """Logs runtime."""

  def begin(self):
    self._step = -1
    self._start_time = time.time()
    self._last_log_time = time.time()

  def before_run(self, run_context):
    self._step += 1

  def after_run(self, run_context, run_values):
    if self._step % LOG_FREQ == 0:
      current_time = time.time()
      delta_time = current_time - self._last_log_time
      self._last_log_time = current_time

      examples_per_sec = LOG_FREQ * BATCH_SIZE / delta_time
      sec_per_batch = float(delta_time / LOG_FREQ)

      format_str = ('%s: step %d - %.1f examples/sec, %.3f '
                    'sec/batch)')
      print (format_str % (datetime.now(), self._step,
                           examples_per_sec, sec_per_batch))

def output_dir(base_dir, num_blocks, plain):
    """Constructs the path to which files are written.

    Args:
        base_dir: Path the the base directory.
        num_blocks: Int indicating the number of building blocks used.
        plain: Boolen indicating whether the model is a plain model ('True')
               or a residual model ('False').

    Returns:
        The path to which files are written.
    """
    model_name = ("model" + str(num_blocks) +
                  {True: "plain", False: "residual"}[plain])
    return os.path.join(base_dir, model_name)

def train(num_blocks, plain, input_dir, batch_size, num_epochs):
    """Train a plain or residual model with specified number of building blocks
    per level.

    Args:
        num_blocks: Int indicating the number of building blocks to use per
                    level of building blocks.
        plain: Boolean indicating whether to use a plain model ('True') or a
               residual model ('False').
        input_dir: Path to input data files.
        batch_size: Int indicating the batch size to use during training.
        num_epochs: Int indicating the number of epochs performed during
                    training.
    """
    global_num_iter = tf.contrib.framework.get_or_create_global_step()

    # First of all, we set up the model to train.
    train_images, train_labels = data.input(input_dir, batch_size,
                                            shuffle = True,
                                            data_type = "train",
                                            num_epochs = num_epochs)
    train_inference = m.inference(train_images, num_blocks, plain = plain)
    train_loss = m.loss(train_inference, train_labels)
    train_op = m.train(train_loss, global_num_iter)
    train_accuracy = tf.reduce_mean(tf.cast(m.top_k(train_inference,
                                                    train_labels, 1),
                                            FLOAT))
    tf.summary.scalar("training accuracy", train_accuracy)

    # Now we do the very same thing for the validation of the model. Note that
    # we have to set up a complete new graph due to batch normalization. Note
    # further, that both graphs - the training and the validation graph - have
    # to share their variables!
    tf.get_variable_scope().reuse_variables()
    val_images, val_labels = data.input(input_dir, NUM_VAL_RECORDS,
                                        shuffle = False,
                                        data_type = "val",
                                        num_epochs = None)
    val_inference = m.inference(val_images, num_blocks, plain, mode_eval = True)
    val_accuracy = tf.reduce_mean(tf.cast(m.top_k(val_inference,
                                                  val_labels, 1),
                                          FLOAT))
    tf.summary.scalar("validation accuracy", val_accuracy)

    # At this point, we're done with the model. To monitor training we set up
    # some Hooks before starting to train though.
    saver = tf.train.Saver()
    saver_hook = tf.train.CheckpointSaverHook(
            output_dir(CHECKPOINT_DIR, num_blocks, plain),
            save_steps = 1000, saver = saver)

    log_hook = _RuntimeLoggerHook()

    summary_writer = tf.summary.FileWriter(
            output_dir(LOG_DIR, num_blocks, plain),
            tf.get_default_graph())
    summary_op = tf.summary.merge_all()
    summary_hook = tf.train.SummarySaverHook(save_steps = 10,
                                             summary_writer = summary_writer,
                                             summary_op = summary_op)

    hooks = [log_hook, saver_hook, summary_hook]
    with tf.train.MonitoredSession(hooks = hooks) as sess:
        while not sess.should_stop():
            sess.run(train_op)

if __name__ == "__main__":
    from sys import argv

    num_blocks = int(argv[1])
    plain = {"plain": True, "res": False}[argv[2]]

    train(num_blocks, plain, PREPROCESSED_DIR, BATCH_SIZE, NUM_EPOCHS)
