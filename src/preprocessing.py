import tensorflow as tf
import cifar10_input as cifar10
from config import *
from global_vars import *
import os

def _int64_feature(value):
    """Converts the specified Int64 into a tf.train.Feature suitable for use
    with tf.train.Example.

    Args:
        value: Int representing the feature.

    Returns:
        tf.train.Feature representing the feature.
    """
    return tf.train.Feature(int64_list = tf.train.Int64List(value = [value]))

def _byte_string_feature(value):
    """Converts the specified byte string into a tf.train.Feature suitable for
    use with tf.train.Example.

    Args:
        value: Byte string representing the feature.

    Returns:
        tf.train.Feature representing the feature.
    """
    return tf.train.Feature(bytes_list = tf.train.BytesList(value = [value]))

def _cifar10_example(image, label, data_format):
    """Converts the specified raw image data and corresponding ground truth
    label into a tf.train.Example representing a CIFAR10 record.

    Args:
        image: 4D float numpy tensor representing the image; Must be of shape
               [IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS] or
               [IMAGE_CHANNELS, IMAGE_HEIGHT, IMAGE_WIDTH], depending on the data
               format.
        label: Int representing the corresponding ground truth label.
        data_format: String in "HWC", "CHW" indicating the data format.

    Returns:
        tf.train.Example representing a CIFAR10 record.
    """
    image_raw = image.tostring()
    return tf.train.Example(features = tf.train.Features(feature = {
        "height": _int64_feature(IMAGE_SIZE),
        "width": _int64_feature(IMAGE_SIZE),
        "depth": _int64_feature(IMAGE_CHANNELS),
        "format": _byte_string_feature(data_format.encode("utf-8")),
        "image_raw": _byte_string_feature(image_raw),
        "label": _int64_feature(label)}))

def _write_batch(images, labels, data_format, filename):
    """Writes a batch of records to the specified '.tfrecords' file.

    Args:
        images: Float numpy tensor representing the images; Must be of shape
                [batch_size, IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS] or
                [batch_size, IMAGE_CHANNELS, IMAGE_HEIGHT, IMAGE_WIDTH], depending on the data
                format.
        labels: Int tensor representing the corresponding ground truth labels;
                Must be of shape [batch_size].
        data_format: String in "NHWC", "NCHW" indicating the data format.
        filename: String indicating the path and filename of the file to write.
    """
    num_records = images.shape[0]

    if num_records != labels.shape[0]:
        raise ValueError("_write_batch: Number of records in images (%d) and "
                         "in labels (%d) not consistent."
                         % (images.shape[0], labels.shape[0]))

    print("Writing", filename)
    with tf.python_io.TFRecordWriter(filename) as writer:
        for index in range(num_records):
            writer.write(_cifar10_example(images[index],
                                          labels[index],
                                          data_format[1:]).SerializeToString())

def _getFilenameQueue(data, num_epochs = None):
    """Returns a filename queue holding the training, validation or test data
    filenames.

    Args:
        data: String in "train", "val" and "test" indicating which data set to
              load.
        num_epochs: Int indicating how often every record should be produced
                    before raising an 'OutOfRangeError'. 'None' disables this
                    upper bound.

    Returns:
        A filename queue holding the data filenames.
    """
    filename = {"train": FILENAME_TRAINING, "val": FILENAME_VALIDATION,
                "test": FILENAME_TEST}[data]
    filename = os.path.join(INPUT_DIR, filename)

    if not tf.gfile.Exists(filename):
        raise ValueError("Unable to locate " + filename)

    queue = tf.train.string_input_producer([filename], num_epochs = num_epochs)
    return queue

def generate_preprocessed_data(input_dir, output_dir, num_train_records,
                               num_prep_train_records, num_test_records,
                               records_per_train_file):
    """Preprocess the CIFAR10 binary data and write the processed data to
    .tfrecords-files.

    Preprocessing consists of per pixel mean subtraction. The mean is determined
    on the training data only and later subtracted from the validation and the
    test data as well.

    Args:
        input_dir: String representing the directory where the binary CIFAR10
                   data is located.
        output_dir: String representing the directory where to place the
                    .tfrecords-files.
        num_train_records: Int indicating the number of train records found in
                           the CIFAR10 data to be used.
        num_prep_train_records: Int indicating the number of preprocessed train
                                records written to .tfrecords-files. The
                                remaining records of CIFAR10 training data are
                                used as validation data.
        num_test_records: Int indicating the number of test records found in
                          CIFAR10 data to be used.
        records_per_train_file: Int indicating the number of records per
                                .tfrecords-file.
                                num_prep_train_records must be even divisible by
                                records_per_train_file.
    """
    # First, we read in the binary training data and the test data. The former
    # is splitted into a training and a validation fold.
    binary_train_images, binary_train_labels = cifar10.input(input_dir,
                                                     num_train_records,
                                                     num_epochs = 1)
    training_images = binary_train_images[:num_prep_train_records]
    validation_images = binary_train_images[num_prep_train_records:]
    training_labels = binary_train_labels[:num_prep_train_records]
    validation_labels = binary_train_labels[num_prep_train_records:]

    test_images, test_labels = cifar10.input(input_dir, num_test_records,
                                     test_data = True, num_epochs = 1)

    # Now, we compute the per pixel mean over the training images and subtract
    # it from all three batches.
    per_pixel_mean = tf.reduce_mean(training_images, axis = 0)
    training_preprocessed = training_images - per_pixel_mean
    validation_preprocessed = validation_images - per_pixel_mean
    test_preprocessed = test_images - per_pixel_mean

    # Before writing everything to files, we have to initialize local and global
    # variables as well as instanciate a session since our input data is fetched
    # using readers.
    global_init_op = tf.global_variables_initializer()
    local_init_op = tf.local_variables_initializer()

    sess = tf.Session()
    sess.run([global_init_op, local_init_op])

    # Start input enqueue threads.
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    try:
        # Evaluate the tensors defined above.
        results = sess.run({"training_preprocessed": training_preprocessed,
                            "training_labels": training_labels,
                            "validation_preprocessed": validation_preprocessed,
                            "validation_labels": validation_labels,
                            "test_preprocessed": test_preprocessed,
                            "test_labels": test_labels})
        test_preprocessed = results["test_preprocessed"]
        test_labels = results["test_labels"]

        tf.gfile.MakeDirs(output_dir)

        # Finally we write the preprocessed data to files.
        for index in range(num_prep_train_records // records_per_train_file):
            start = index * records_per_train_file
            end = start + records_per_train_file
            filename = TRAIN_BASENAME + str(index) + FILE_SUFFIX
            _write_batch(results["training_preprocessed"][start : end],
                         results["training_labels"][start : end],
                         "NHWC",
                         os.path.join(output_dir, filename))

        filename = VAL_BASENAME + FILE_SUFFIX
        _write_batch(results["validation_preprocessed"],
                     results["validation_labels"],
                     "NHWC",
                     os.path.join(output_dir, filename))

        filename = TEST_BASENAME + FILE_SUFFIX
        _write_batch(results["test_preprocessed"], results["test_labels"],
                     "NHWC",
                     os.path.join(output_dir, filename))
        print("Preprocessing done.")

    except tf.errors.OutOfRangeError as e:
        print("This error should not have been reached! Contrary to the "
              "situation during training!")
        raise e

    finally:
        # When done, ask the threads to stop.
        coord.request_stop()

    # Wait for threads to finish.
    coord.join(threads)
    sess.close()

if __name__ == "__main__":
    generate_preprocessed_data(INPUT_DIR, PREPROCESSED_DIR, NUM_TRAIN_RECORDS,
                               NUM_PREP_TRAIN_RECORDS, NUM_TEST_RECORDS,
                               RECORDS_PER_TRAIN_FILE)
