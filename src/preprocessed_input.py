import tensorflow as tf
import os
from global_vars import *
from config import *
from cifar10_input import CIFAR10Record

def _getFilenameQueue(input_dir, data_type, num_epochs):
    """Returns a filename queue holding the training, validation or test data
    filenames.

    Args:
        input_dir: The directory containing the preprocessed data.
        data_type: String indicating which data to load; Must be in "train",
                   "val", "test".
        num_epochs: Int indicating how often every record should be produced
                    before raising an 'OutOfRangeError'. 'None' disables this
                    upper bound.

    Returns:
        A filename queue holding the data filenames.
    """
    if data_type == "train":
        filenames = [os.path.join(input_dir, TRAIN_BASENAME + str(i) +
                                             FILE_SUFFIX)
                     for i in range(NUM_PREP_TRAIN_RECORDS //
                                    RECORDS_PER_TRAIN_FILE)]
    elif data_type == "val":
        filenames = [os.path.join(input_dir, VAL_BASENAME + FILE_SUFFIX)]
    elif data_type == "test":
        filenames = [os.path.join(input_dir, TEST_BASENAME + FILE_SUFFIX)]
    else:
        raise ValueError("_getFilenameQueue: invalid type of data specified.")

    for f in filenames:
        if not tf.gfile.Exists(f):
            raise ValueError("Unable to locate " + f)

    queue = tf.train.string_input_producer(filenames, num_epochs = num_epochs)
    return queue

def _readRecord(queue):
    """Reads a single CIFAR10 record from the specified queue.

    Args:
        queue: The queue to read from.

    Returns:
        A single CIFAR10 record.
    """
    reader = tf.TFRecordReader()
    _, serialized_record = reader.read(queue)
    features = tf.parse_single_example(serialized_record, features = {
        "format": tf.FixedLenFeature([], tf.string),
        "image_raw": tf.FixedLenFeature([], tf.string),
        "label": tf.FixedLenFeature([], tf.int64)})
    image = tf.decode_raw(features["image_raw"], FLOAT)
    # TODO: fix
    # data_format = features["format"].decode("utf-8")
    data_format = "HWC"
    shape = {"HWC": [IMAGE_SIZE, IMAGE_SIZE, IMAGE_CHANNELS],
             "CHW": [IMAGE_CHANNELS, IMAGE_SIZE, IMAGE_SIZE]}[data_format]
    image = tf.reshape(image, shape)
    label = features["label"]
    label = tf.reshape(tf.cast(label, tf.int32), [1])
    return CIFAR10Record(label, image, data_format)

def input(input_dir, batch_size, shuffle,
          data_type, num_epochs):
    """Construct CIFAR10 preprocessed input using readers.

    Args:
        input_dir: The directory containing the preprocessed data.
        batch_size: Int indicating the number of records per batch.
        shuffle: Boolean indicating whether to shuffle the records read from the
                 input files or not.
        data_type: String indicating which data to load; Must be in "train",
                   "val", "test".
        num_epochs: Optional integer indicating the maximum number of epochs.
                    None disables this upper bound

    Returns:
        images: 4D tensor of shape
                [batch_size, IMAGE_SIZE, IMAGE_SIZE, IMAGE_CHANNELS]
                representing the images.
        labels: 1D tensor of shape [batch_size] representing the corresponding
                ground truth.
    """
    queue = _getFilenameQueue(input_dir, data_type, num_epochs)
    record = _readRecord(queue)

    num_records = {"train": NUM_PREP_TRAIN_RECORDS,
                   "val": NUM_VAL_RECORDS,
                   "test": NUM_TEST_RECORDS}[data_type]
    min_after_dequeue = int(MIN_PREFETCHED_FRACTION * num_records)
    capacity = min_after_dequeue + MAX_PREFETCHED_BATCHES * batch_size

    if shuffle:
        images, labels = tf.train.shuffle_batch(
                [record.image, record.label],
                batch_size = batch_size,
                num_threads = NUM_PREPROCESS_THREADS,
                min_after_dequeue = min_after_dequeue,
                capacity = capacity)
    else:
        images, labels = tf.train.batch(
                [record.image, record.label],
                batch_size = batch_size,
                num_threads = NUM_PREPROCESS_THREADS,
                capacity = capacity)

    return images, tf.reshape(labels, [batch_size])
