"""Configure runtime settings of the experiments.

This module defines the globals used throughout all modules conserning general
runtime settings like input data directories, ...
"""

import tensorflow as tf

"""The directory containing the binary CIFAR10 data.
"""
INPUT_DIR = "./../cifar10_data/"

"""The directory containing checkpoint files.
"""
CHECKPOINT_DIR = "../training/"

"""The directory containing the preprocessed data.
"""
PREPROCESSED_DIR = "../preprocessed/"

"""The directory containing the logged summaries.
"""
LOG_DIR = "../log/"

"""The precision of calculations.
"""
FLOAT = tf.float32

"""The data representation format. Has to be in "NCHW" and "NHWC".
Currently, only 'NCHW' is supported.

The CHW representation of an image is given by a 3D tensor where the outermost
index iterates over the image's channels. The subsequent indices iterate over
the rows and columns respectively.
The HWC representation is given by a 3D tensor where the indices iterate over
rows, columns and channels respectively.
"""
DATA_FORMAT = "NCHW"

"""Threads used for preprocessing and file reading tasks.
"""
NUM_PREPROCESS_THREADS = 3
NUM_PROCESS_THREADS = 5

"""Logging frequency measured in steps per log entry.
"""
LOG_FREQ = 100
