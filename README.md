# README

Welcome to my "seminar CIFAR10 experiments"-repository!

The source code you encounter here belongs to the experiments I conducted during
my seminar thesis in the "Hauptseminar Medieninformatik" course offered by Prof.
Granitzer at the University of Passau during the summer term 2017.

## Getting started

In order to reproduce the experiments cited in my seminar thesis, please perform
the following steps:

### Requirements

 1. Clone this repository via `git clone https://kangaroo-penguin@bitbucket.org/kangaroo-penguin/seminar-experiments.git`
 2. Get yourself a copy of the [CIFAR 10][cifar] _binary_ data and extract it to
    your destination of choice.
 3. Make sure you have the python [TensorFlow][tensor-flow] framework installed.

### Training models

 1. Check and adapt the settings in `src/config.py` to your setup.
 2. Run `python preprocessing.py` from within the source-directory to preprocess
    the binary data converting it into `.tfrecords` files. This command has to
    be issued before proceeding! It has to be issued only once though.
 3. Run `python train.py num_building_blocks model_type` in order to train a
    model. `num_building_blocks` has to be replaced by the number of building
    blocks to be used per level of building blocks. `model_type` can either be
    `plain` or `res` to train a plain or residual model respectively.

### Evaluating performance

 1. Issue `tensorboard --logdir=path/to/logs`. `path/to/logs` has to be replaced
    by the path specified in `LOG_DIR` in `src/config.py` or one of its
    subfolders.
 2. Point your browser of choice at `localhost:6006`. See [this][tensor-board]
    for details.
 3. In order to examine the final performance of a trained model on the CIFAR10
    test set, issue `python eval.py num_building_blocks model_type` from within
    `src`. Note that the specified model has to be trained before!

 [cifar]: https://www.cs.toronto.edu/~kriz/cifar.html
 [tensor-flow]: https://www.tensorflow.org/
 [tensor-board]: https://www.tensorflow.org/get_started/summaries_and_tensorboard#launching_tensorboard

## Disclaimer

This software is provided "as is" and any expressed or implied warranties,
including, but not limited to, the implied warranties of merchantability and
fitness for a particular purpose are disclaimed. In no event shall the regents
or contributors be liable for any direct, indirect, incidental, special,
exemplary, or consequential damages (including, but not limited to, procurement
of substitute goods or services; loss of use, data, or profits; or business
interruption) however caused and on any theory of liability, whether in
contract, strict liability, or tort (including negligence or otherwise) arising
in any way out of the use of this software, even if advised of the possibility
of such damage.

Furthermore, note that this is my very first project I implemented in Python.
There might be violations of coding conventions and alike all over the place.
